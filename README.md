# Fuzzy Search Experiment

This application allows you to search through a list of countries (from an API), in a fuzzy way.

Check out the live version here: [antexperiments.gitlab.io/hello-world/fuzzy-search](https://antexperiments.gitlab.io/hello-world/fuzzy-search/)